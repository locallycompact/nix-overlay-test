{stdenv, autoconf, bash, gnutar, gzip, gnumake, gcc, binutils-unwrapped, coreutils, gawk, gnused, gnugrep}:

stdenv.mkDerivation {
  name = "hello";
  configurePhase = ''
    ./configure --prefix $out
  '';
  buildPhase = ''
    make
  '';
  installPhase = ''
    make install
  '';
  buildInputs = [ autoconf gnutar gzip gnumake gcc binutils-unwrapped coreutils gawk gnused gnugrep ];
  src = fetchGit {
    url = https://github.com/avar/gnu-hello;
    ref = "master";
    rev = "c45fac5f716c9dd298c0a7ae9436538e622cd0f3";
  };
  system = builtins.currentSystem;
}
