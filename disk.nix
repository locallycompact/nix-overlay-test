let
  pkgs = import <nixpkgs> { };
  lib = pkgs.lib;
  x86_config = { pkgs, ... }:
  {  
    nixpkgs.overlays = [ (import ./layer.nix) ];
    fileSystems = {
      "/" = { device = "LABEL=nixos"; fsType = "ext4"; };
    };
    boot = {
      loader.grub = {
        enable = true;
        device = "/dev/vda";
      };
    };
    services.mingetty.autologinUser = "root";
    environment.systemPackages = [ pkgs.stack pkgs.cargo pkgs.foo ];
  };
  x86_build = import <nixpkgs/nixos> { system = "x86_64-linux"; configuration = x86_config; };
  rootDisk = import <nixpkgs/nixos/lib/make-disk-image.nix> {
    name = "nix_x86_rootfs";
    inherit pkgs lib;
    config = x86_build.config;
    diskSize = 4096;
  };
in rootDisk
